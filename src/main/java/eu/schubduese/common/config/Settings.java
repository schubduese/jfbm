package eu.schubduese.common.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Properties;

import eu.schubduese.common.utils.StringUtils;

public class Settings
{

	private static Properties 		properties;
	private static File 			propertiesFile;
	
	public static String loadValue(String cnfname, String defaultValue)
	{
		String s = loadValue(cnfname);
		if (StringUtils.isEmpty(s))
			return defaultValue;
		else
			return s;
	}
	
	public static String loadValue(String value)
	{
		return getProperties().getProperty(value);
	}
	
	
	public static int loadIntValue(String cnfName, int defaultValue)
	{
		int i = loadIntValue(cnfName);
		if (i == -1)
			return defaultValue;
		else
			return i;
	}
	
	public static boolean loadBoolValue(String cnfName, boolean defaultValue)
	{
		int i = loadIntValue(cnfName);
		if (i == -1)
			return defaultValue;
		else 
			return i != 0;
	}
	
	public static boolean loadBoolValue(String cnfName)
	{
		int i = loadIntValue(cnfName);
		return i == 1;
	}
	
	public static int loadIntValue(String cnfValue)
	{
		String value = getProperties().getProperty(cnfValue);
		if (StringUtils.isNotEmpty(value))
		{
			try
			{
				return Integer.parseInt(value);
			}
			catch (NumberFormatException e) 
			{
				//hier nichts machen
			}
		}
		return -1;
	}
	
	public static void setBoolValue(String cnfName, boolean value)
	{
		setValue(cnfName, value ? "1": "0");
	}

	public static void setValue(String cnfName, String value)
	{
		if (StringUtils.isNotEmpty(value))
			getProperties().setProperty(cnfName, value);
	}

	private static Properties getProperties()
	{
		if (properties == null)
		{
			try
			{
				FileReader reader = new FileReader(getPropertiesFile());
				properties = new Properties();
				properties.load(reader);
				reader.close();
			} catch (IOException ex)
			{
				System.err.println(ex);
			}

		}
		return properties;
	}

	public static boolean save()
	{
		try
		{
			OutputStream outputStream = new FileOutputStream(getPropertiesFile());
			Writer writer = new OutputStreamWriter(outputStream, Charset.forName("UTF-8"));
			getProperties().store(writer, "Configdatei für den Java Fritzbox Monitor");
			writer.close();
			return true;
		} catch (IOException e)
		{
			System.err.println(e);
		}
		return false;
	}
	
	private static File getPropertiesFile()
	{
		if (propertiesFile == null)
		{
			propertiesFile = new File(System.getProperty("user.home") + "/" + ".jfbm");
			if (!propertiesFile.exists())
			{
				try
				{
					propertiesFile.createNewFile();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
				
			}
		}
		return propertiesFile;
	}

}
