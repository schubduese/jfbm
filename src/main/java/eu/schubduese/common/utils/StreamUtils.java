package eu.schubduese.common.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StreamUtils 
{

	public void bufferedReader2File(BufferedReader bufferedReader, String filename)
	{
		BufferedWriter writer;
		try {
			writer = Files.newBufferedWriter(new File(filename).toPath(), StandardCharsets.UTF_8);
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				writer.write(line);
				writer.newLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
