package eu.schubduese.common.gui.elements;

import eu.schubduese.common.config.Settings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;

public class SaveCheckBox extends CheckBox
{
	
	public SaveCheckBox()
	{
		super();
	}
	
	
	public SaveCheckBox(String cnfName)
	{
		super();
		init(cnfName);
	}
	
	public void init(String cnfName)
	{
		setSelected(Settings.loadBoolValue(cnfName));
		selectedProperty().addListener(new ChangeListener<Boolean>() 
		{
			
		    @Override
		    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue)
		    {
		        Settings.setBoolValue(cnfName, isSelected());
		    }
		    
		});

	}
}
