package eu.schubduese.fritzfx.gui;

import java.io.File;
import java.io.IOException;

import eu.schubduese.common.config.Settings;
import eu.schubduese.common.gui.elements.SaveCheckBox;
import eu.schubduese.common.guitools.GuiTools;
import eu.schubduese.common.utils.FxImageTools;
import eu.schubduese.common.utils.ListUtils;
import eu.schubduese.common.utils.StringUtils;
import eu.schubduese.fritzfx.data.VCardFileImporter;
import eu.schubduese.fritzfx.data.VCardPerson;
import eu.schubduese.fritzfx.fritz.FritzConnector;
import eu.schubduese.fritzfx.fritz.IFritzboxConnector;
import eu.schubduese.fritzfx.fritz.WanStatusInformation;
import eu.schubduese.fritzfx.gui.MainGuiController.PropKey;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class MainGuiController implements IFritzboxConnector
{
                                                              
	public enum PropKey
	{
	
		FRITZBOXUSER("fritzboxuser"),
		FRITZBOXPASSWORT("fritzboxpasswort"),
		FRITZBOXIP("fritzboxip"),
		FRITZBOXPORT("fritzboxport"),
		FRITZBOXAUTOCONNECT("fritzboxautoconnect"),
		FRITZBOXACTRATE("fritzboxactrate");
	
		private String propertiesValue;
	
		PropKey(String propertiesValue)
		{
			this.propertiesValue = propertiesValue;
		}
	
		public String getPropertiesValue()
		{
			return propertiesValue;
		}
	
	}

	@FXML TableView<VCardPerson> 	    ocTableView;            
	@FXML TableView<VCardPerson> 	    fritzTableView;            
	@FXML Button 					    loadBt;               
	@FXML Button 					    exportBt;             
	private Stage 					    primaryStage;         
	@FXML Button 					    configBt;             
	@FXML Button 					    btImport;             
	@FXML MenuItem 					    onMenuSettings;       
	@FXML MenuItem 					    onMenuClose;          
	@FXML Button 					    btSave;               
	@FXML Button 					    btCancel;             
	@FXML TextField 				    textFieldIp;          
	@FXML TextField 				    textFieldUsername;    
	@FXML Button 					    btConnect;            
	@FXML TextField 				    textFieldPort;        
	@FXML TextField 				    owncloudUrl;          
	@FXML TextField					    owncloudUsername;     
	@FXML PasswordField                 passwordFieldFritz;   
	private FritzConnector			    fritzConnector;       
	@FXML Label     				    statusLabel;          
	@FXML Label 					    bandwidthLabel;       
	@FXML TextField 				    textFieldActRate;     
	private WanChartCreator				wanChartCreator;             
	@FXML AreaChart<Integer, Integer> 	wanMonitorChart;
	@FXML TabPane 						mainTabPane;
	@FXML Tab 							wanMonitorTab;
	private SysTray						sysTray;
	@FXML Button 						loadFritzBt;
	@FXML Tab 							anrufListePane;
	@FXML TableView 					anrufTableView;
	@FXML NumberAxis 					wanMonitorXAxis;
	@FXML NumberAxis 					wanMonitorYAxis;
	@FXML ImageView 					imageviewStatus;
	@FXML SaveCheckBox 					autoconnectSaveCb;
	@FXML Button 						loadCallListBt;
	@FXML MenuItem 						menuItemConnect;
	@FXML MenuItem 						menuItemDisconnect;
	private FxImageTools				fxImageTools;

	@FXML
    public void initialize() 
	{
		ocTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		ocTableView.getSelectionModel().selectedItemProperty().addListener(e -> selectionChanged());
		loadSettings();
		setStatusBar(null);
		getFritzConnector().addIFritzboxConnector(this);
		getFritzConnector().addIFritzboxConnector(getWanChartCreator());
		getFritzConnector().addIFritzboxConnector(getSysTray());
		autoconnectSaveCb.init(MainGuiController.PropKey.FRITZBOXAUTOCONNECT.getPropertiesValue());
		if (Settings.loadBoolValue(MainGuiController.PropKey.FRITZBOXAUTOCONNECT.getPropertiesValue()))
			doConnect();
		else
			menuItemDisconnect.setDisable(true);
    }
	
	private void selectionChanged()
	{
		exportBt.setDisable(ListUtils.isEmpty(ocTableView.getSelectionModel().getSelectedItems()));
	}

	@FXML 
	public void onBtLoad() 
	{
		String fileName = null; 
//		fileName = "/home/joerg/develop/eclipseworkspace/vcardfx/data/Contacts.vcf";
		FileChooser fileChooser = new FileChooser();
		File file = fileChooser.showOpenDialog(primaryStage);
        if (file != null) 
        	fileName = file.getAbsolutePath();
		VCardFileImporter cardFileImporter = new VCardFileImporter(fileName);
		if (cardFileImporter.parseFile())
		{
			ObservableList<VCardPerson> observableList = FXCollections.observableArrayList(cardFileImporter.getPersonenListe());
			ocTableView.setItems(observableList);
		}
	}

	@FXML 
	public void onBtExport() 
	{
		
	}

	@FXML 
	public void onBtSettings() 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("SettingsDialog.fxml"));
		Parent root;
		try 
		{
			root = (Parent)loader.load();
			Scene scene = new Scene(root);        
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setPrimaryStage(Stage primaryStage) 
	{
		this.primaryStage = primaryStage;
	}

	@FXML public void onBtImport() 
	{
		
	}

	@FXML public void onMenuClose() 
	{
		close();
	}
	
	protected void close()
	{
		Settings.save();
		primaryStage.close();
		Platform.exit();
		System.exit(0);
	}

	@FXML 
	public void onCancel() {}
	
	@FXML 
	public void doConnect() 
	{
		if (getFritzConnector().isConnected())
		{
			getFritzConnector().disconnect();
		}
		else
		{
			if (StringUtils.isEmpty(Settings.loadValue(MainGuiController.PropKey.FRITZBOXIP.getPropertiesValue()))
					|| StringUtils.isEmpty(Settings.loadValue(MainGuiController.PropKey.FRITZBOXUSER.getPropertiesValue())))
			{
				GuiTools.showMessageDialog("Einstellungen leer", "Es ist weder eine IP-Adresse oder ein Benutzername in den Einstellungen eingetragen worden.");
				return;
			}
			getFritzConnector().connect();
		}
		menuItemConnect.setDisable(getFritzConnector().isConnected());
		menuItemDisconnect.setDisable(!getFritzConnector().isConnected());
	}
	
	@FXML public void doDisconnect() 
	{
		if (getFritzConnector().isConnected())
			getFritzConnector().disconnect();
	}
	
	private void loadSettings()
	{
		textFieldIp.setText(Settings.loadValue(MainGuiController.PropKey.FRITZBOXIP.getPropertiesValue()));
		passwordFieldFritz.setText(Settings.loadValue(MainGuiController.PropKey.FRITZBOXPASSWORT.getPropertiesValue()));
		textFieldUsername.setText(Settings.loadValue(MainGuiController.PropKey.FRITZBOXUSER.getPropertiesValue()));
		textFieldActRate.setText(Settings.loadValue(MainGuiController.PropKey.FRITZBOXACTRATE.getPropertiesValue()));
	}
	
	@FXML public void onSave()
	{
		Settings.setValue(MainGuiController.PropKey.FRITZBOXIP.getPropertiesValue(), textFieldIp.getText());
		Settings.setValue(MainGuiController.PropKey.FRITZBOXPASSWORT.getPropertiesValue(), passwordFieldFritz.getText());
		Settings.setValue(MainGuiController.PropKey.FRITZBOXUSER.getPropertiesValue(), textFieldUsername.getText());
		Settings.setValue(MainGuiController.PropKey.FRITZBOXACTRATE.getPropertiesValue(), textFieldActRate.getText());
		Settings.setValue(MainGuiController.PropKey.FRITZBOXPORT.getPropertiesValue(), textFieldPort.getText());
		Settings.save();
		getFritzConnector().setValues();
	}

	private FritzConnector getFritzConnector()
	{
		
		if (fritzConnector == null)
		{
			fritzConnector = new FritzConnector();
		}
		return fritzConnector;
	}
	
	@Override
	public void SetConnected(boolean isConnected)
	{
		 //nichts machen
	}

	@Override
	public void setStatusInformation(WanStatusInformation wanStatusInformation)
	{
		Platform.runLater(new Runnable()
		{
			
			@Override
			public void run()
			{
				setStatusBar(wanStatusInformation);
			}

		});
	}

	private void setStatusBar(WanStatusInformation wanStatusInformation)
	{
		if (wanStatusInformation == null)
		{
			bandwidthLabel.setText(null);
			statusLabel.setText("Verbindung zur Fritz-Box nicht hergestellt");
			imageviewStatus.setImage(getFxImageTools().loadImage("redpoint.png"));
		}
		else
		{
			bandwidthLabel.setText(wanStatusInformation.createDlUpStatusString());
			statusLabel.setText(wanStatusInformation.createCommonInfo());
			if (wanStatusInformation.isWanConntected())
				imageviewStatus.setImage(getFxImageTools().loadImage("greenpoint.png"));
			else
				imageviewStatus.setImage(getFxImageTools().loadImage("redpoint.png"));
		}
	}
	
	public WanChartCreator getWanChartCreator()
	{
		if (wanChartCreator == null)
			wanChartCreator = new WanChartCreator(wanMonitorChart, this);
		return wanChartCreator;
	}

	protected TabPane getMainTabPane()
	{
		return mainTabPane;
	}

	protected Tab getWanMonitorTab()
	{
		return wanMonitorTab;
	}

	protected Stage getPrimaryStage()
	{
		return primaryStage;
	}

	protected NumberAxis getWanMonitorXAxis()
	{
		return wanMonitorXAxis;
	}

	protected NumberAxis getWanMonitorYAxis()
	{
		return wanMonitorYAxis;
	}

	protected SysTray getSysTray()
	{
		if (sysTray == null)
			sysTray = new SysTray(this);
		return sysTray;
	}

	@FXML 
	public void onBtCallList() 
	{
		getFritzConnector().getFritzAnrufListe();
	}

	protected FxImageTools getFxImageTools()
	{
		if (fxImageTools == null)
			fxImageTools = new FxImageTools();
		return fxImageTools;
	}

}
