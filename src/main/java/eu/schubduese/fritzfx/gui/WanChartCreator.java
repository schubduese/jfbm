package eu.schubduese.fritzfx.gui;

import eu.schubduese.fritzfx.fritz.IFritzboxConnector;
import eu.schubduese.fritzfx.fritz.WanStatusInformation;
import javafx.application.Platform;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;

public class WanChartCreator implements IFritzboxConnector
{

	private AreaChart<Integer, Integer> 	areaChart;
	private MainGuiController			 	mainGuiController;
	private boolean							isFirstTimeLoaded;
	
	public WanChartCreator(AreaChart<Integer, Integer> areaChart, MainGuiController mainGuiController)
	{
		this.areaChart = areaChart;
		this.mainGuiController = mainGuiController;
		isFirstTimeLoaded = true;
	}

	@Override
	public void SetConnected(boolean isConnected)
	{
		// nichts tun
		
	}

	@Override
	public void setStatusInformation(WanStatusInformation wanStatusInformation)
	{
		if (mainGuiController.getMainTabPane().getSelectionModel().getSelectedItem() != mainGuiController.getWanMonitorTab())
			return;
		areaChart.setAnimated(isFirstTimeLoaded);
		if (isFirstTimeLoaded)
			isFirstTimeLoaded = false;
		Series<Integer, Integer> seriesDl = new Series<>();
		Series<Integer, Integer> seriesUl = new Series<>();
		seriesDl.setName("Download-Rate");
		seriesUl.setName("Upload-Rate");
		//TODO: funktioniert noch nicht richtig
//		mainGuiController.getWanMonitorYAxis().setAutoRanging(false);
//		mainGuiController.getWanMonitorYAxis().setLowerBound(0);
//		mainGuiController.getWanMonitorYAxis().setUpperBound(wanStatusInformation.getMaxDownloadBitrate());
//		mainGuiController.getWanMonitorYAxis().setTickUnit(0.1);
		
		int counter = 0;
		for (Integer value : wanStatusInformation.getBytesReceivedListe())
		{
			seriesDl.getData().add(new XYChart.Data<Integer, Integer>(counter, value));
			counter++;
		}
		counter = 0;
		for (Integer value : wanStatusInformation.getBytesSendListe())
		{
			seriesUl.getData().add(new XYChart.Data<Integer, Integer>(counter, value));
			counter++;
		}
		Platform.runLater(new Runnable()
		{
			
			@Override
			public void run()
			{
				areaChart.getData().clear();
				areaChart.getData().addAll(seriesDl);
				areaChart.getData().addAll(seriesUl);
			}
		});
		
	}
	
	
	
}
