package eu.schubduese.fritzfx.gui;

import java.awt.AWTException;
import java.awt.Font;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.net.URL;

import eu.schubduese.fritzfx.fritz.IFritzboxConnector;
import eu.schubduese.fritzfx.fritz.WanStatusInformation;
import javafx.application.Platform;

public class SysTray implements IFritzboxConnector
{

	private MainGuiController			mainGuiController;
	private TrayIcon					trayIcon;

	public SysTray(MainGuiController mainGuiController)
	{
		this.mainGuiController = mainGuiController;
		create();
	}

	private void create()
	{
		try
		{
			java.awt.Toolkit.getDefaultToolkit();

			if (!SystemTray.isSupported())
			{
				System.out.println("Keine Unterstützung von Taskleisten-Icons unter diesem System");
				return;
			}
			SystemTray tray = SystemTray.getSystemTray();
			getTrayIcon()
					.addActionListener(event -> Platform.runLater(() -> mainGuiController.getPrimaryStage().show()));
			MenuItem openItem = new MenuItem("Oberfläche öffnen");
			openItem.addActionListener(event -> Platform.runLater(() -> mainGuiController.getPrimaryStage().show()));
			Font defaultFont = Font.decode(null);
			Font boldFont = defaultFont.deriveFont(Font.BOLD);
			openItem.setFont(boldFont);
			MenuItem exitItem = new MenuItem("Exit");
			exitItem.addActionListener(event -> {
				tray.remove(getTrayIcon());
				Platform.runLater(() -> mainGuiController.close());
			});
			PopupMenu popup = new PopupMenu();
			popup.add(openItem);
			popup.addSeparator();
			popup.add(exitItem);
			getTrayIcon().setPopupMenu(popup);
			tray.add(getTrayIcon());
		} catch (AWTException e)
		{
			e.printStackTrace();
		}
	}

	private TrayIcon getTrayIcon()
	{
		if (trayIcon == null)
		{
			URL url = getClass().getResource("/images/router.png");
			if (url != null)
			{
				Image image = Toolkit.getDefaultToolkit().getImage(url);
				trayIcon = new TrayIcon(image);
			}
			else
				trayIcon = new TrayIcon(Toolkit.getDefaultToolkit().getImage("keinBild"));
		}
		return trayIcon;
	}

	@Override
	public void SetConnected(boolean isConnected)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void setStatusInformation(WanStatusInformation wanStatusInformation)
	{
		getTrayIcon().setToolTip(wanStatusInformation.createDlUpStatusString());
	}

}
