package eu.schubduese.fritzfx.data;

import eu.schubduese.common.utils.StringUtils;

public class VCardPerson 
{

	private String name;
	private String vorname;
	private String telefon;
	private String email;
	
	public VCardPerson()
	{
		super();
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getTelefon()
	{
		return telefon;
	}

	public void setTelefon(String telefon)
	{
		this.telefon = telefon;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getVorname()
	{
		return vorname;
	}

	public void setVorname(String vorname)
	{
		this.vorname = vorname;
	}
	
	protected boolean isNotEmpty()
	{
		return StringUtils.isNotEmpty(name) || 
				StringUtils.isNotEmpty(vorname) ||
				StringUtils.isNotEmpty(telefon) ||
				StringUtils.isNotEmpty(email);
	}
}
