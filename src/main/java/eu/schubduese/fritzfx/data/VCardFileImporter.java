package eu.schubduese.fritzfx.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import eu.schubduese.common.utils.StringUtils;

public class VCardFileImporter 
{
	private static final String BEGIN_VCARD 		= "BEGIN:VCARD";
	private static final String END_VCARD 			= "END:VCARD";
	private static final String NAME 				= "FN:";
	private static final String TELEFON 			= "TEL:";
	private static final String EMAIL 				= "EMAIL:";
	
	private String 				filename;
	private List<VCardPerson> 	personenListe;
		
	public VCardFileImporter(String filename) 
	{
		this.filename = filename;
	}
	
	public boolean parseFile()
	{
		if (StringUtils.isEmpty(filename))
			return false;
		getPersonenListe().clear();
		BufferedReader bufferedReader;
		try
		{
			bufferedReader = new BufferedReader(new FileReader(filename));
			String line = bufferedReader.readLine();
			while (line != null) 
			{
				System.out.println(line);
				if (line.startsWith(BEGIN_VCARD))
					parsePerson(bufferedReader);
				line = bufferedReader.readLine();
			}
			
			bufferedReader.close();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} finally {
		    
		}
		return true;
	}

	private void parsePerson(BufferedReader bufferedReader) throws IOException
	{
		VCardPerson vCardPerson = new VCardPerson();
		String line = bufferedReader.readLine();
		while (line != null) 
		{
			System.out.println(line);
			if (line.startsWith(NAME))
			{
				String splitted[] = line.split(":");
				if (splitted.length > 1)
				{
					String[] splittedNames = splitted[1].split(" ");
					if (splittedNames.length == 2)
					{
						vCardPerson.setVorname(splittedNames[0]);
						vCardPerson.setName(splittedNames[1]);
					}
				}
			}
			else if (line.startsWith(TELEFON))
			{
				String[] splitted = line.split(":");
				if (splitted.length > 0)
					vCardPerson.setTelefon(splitted[splitted.length - 1]);
			}
			else if (line.startsWith(EMAIL))
			{
				String[] splitted = line.split(":");
				if (splitted.length > 0)
					vCardPerson.setEmail(splitted[splitted.length - 1]);
			}
			else if (line.startsWith(END_VCARD))
			{
//				if (vCardPerson.isNotEmpty())
					getPersonenListe().add(vCardPerson);
				break;
			}
			line = bufferedReader.readLine();
		}
		
	}

	public List<VCardPerson> getPersonenListe() 
	{
		if (personenListe == null)
			personenListe = new ArrayList<>();
		return personenListe;
	}

}
