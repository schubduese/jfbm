package eu.schubduese.fritzfx.fritz;

import java.util.ArrayList;
import java.util.List;

import eu.schubduese.common.utils.ListUtils;
import eu.schubduese.common.utils.StringUtils;

public class WanStatusInformation
{

	private List<Integer> 	bytesReceivedListe;
	private List<Integer> 	BytesSendListe;
	private int 			maxDownloadBitrate;
	private int 			maxUploadBitrate;
	private String 			WanAccessType;
	private String 			physicalLinkStatus;
	
	public List<Integer> getCurrentBytesReceived()
	{
		return bytesReceivedListe;
	}
	
	public void setBytesReceivedListe(List<Integer> bytesReceivedListe)
	{
		this.bytesReceivedListe = bytesReceivedListe;
	}

	public List<Integer> getCurrentBytesSend()
	{
		return BytesSendListe;
	}

	public void setBytesSendListe(List<Integer> BytesSendListe)
	{
		this.BytesSendListe = BytesSendListe;
	}
	
	public String createDlUpStatusString()
	{
		StringBuilder sb = new StringBuilder();
		if (ListUtils.isNotEmpty(getBytesReceivedListe()))
		{
			int value = getBytesReceivedListe().get(getBytesReceivedListe().size() - 1) / 1024;
			sb.append(value).append("KBs/");
		}
		if (ListUtils.isNotEmpty(getBytesSendListe()))
		{
			int value = getBytesSendListe().get(getBytesSendListe().size() - 1) / 1024;
			sb.append(value).append("KBs");
		}
		return sb.toString();
	}
	
	public String createCommonInfo()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("WAN-Verbindung: ");
		if (isWanConntected())
		{
			sb.append("verbunden ");
			sb.append("↓").append(getMaxDownloadBitrate()/1000000).append(" Mbit/s ");
			sb.append("↑").append(getMaxUploadBitrate()/1000000).append(" Mbit/s ");
		}
		else
			sb.append("nicht verbunden ");
		return sb.toString();
	}

	public List<Integer> getBytesReceivedListe()
	{
		if (bytesReceivedListe == null)
			bytesReceivedListe = new ArrayList<>();
		return bytesReceivedListe;
	}

	public List<Integer> getBytesSendListe()
	{
		if (BytesSendListe == null)
			BytesSendListe = new ArrayList<>();
		return BytesSendListe;
	}

	public int getMaxDownloadBitrate()
	{
		return maxDownloadBitrate;
	}

	protected void setMaxDownloadBitrate(int maxDownloadBitrate)
	{
		this.maxDownloadBitrate = maxDownloadBitrate;
	}

	protected int getMaxUploadBitrate()
	{
		return maxUploadBitrate;
	}

	protected void setMaxUploadBitrate(int maxUploadBitrate)
	{
		this.maxUploadBitrate = maxUploadBitrate;
	}

	protected String getWanAccessType()
	{
		return WanAccessType;
	}

	protected void setWanAccessType(String wanAccessType)
	{
		WanAccessType = wanAccessType;
	}

	protected String getPhysicalLinkStatus()
	{
		return physicalLinkStatus;
	}

	protected void setPhysicalLinkStatus(String physicalLinkStatus)
	{
		this.physicalLinkStatus = physicalLinkStatus;
	}
	
	public boolean isWanConntected()
	{
		return StringUtils.isNotEmpty(physicalLinkStatus) && (physicalLinkStatus.equals("Up"));
	}
	
}
