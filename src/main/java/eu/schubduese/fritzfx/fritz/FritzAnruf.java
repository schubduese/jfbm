package eu.schubduese.fritzfx.fritz;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="root")
@XmlAccessorType(XmlAccessType.FIELD)
public class FritzAnruf
{
	
	private AnrufTyp 	anrufTyp;
	private Date 		datum;
	private int			dauer;
	
	@XmlElement(name="id")
	private String		id;
	@XmlElement(name="type")
	private int			type;
	@XmlElement(name="caller")
	private int			anrufNr;
	@XmlElement(name="Date")
	private int			dateRaw;

	public enum AnrufTyp
	{
		AUSGEHEND(),
		EINGEHEND_ANGENOMMEN(),
		EINGEHEND_NICHT_ANGENOMMEN();
	}

	protected AnrufTyp getAnrufTyp()
	{
		return anrufTyp;
	}

	protected void setAnrufTyp(AnrufTyp anrufTyp)
	{
		this.anrufTyp = anrufTyp;
	}

	protected Date getDatum()
	{
		return datum;
	}

	protected void setDatum(Date datum)
	{
		this.datum = datum;
	}

	protected int getDauer()
	{
		return dauer;
	}

	protected void setDauer(int dauer)
	{
		this.dauer = dauer;
	}

	
}
