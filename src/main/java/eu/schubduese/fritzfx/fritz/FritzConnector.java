package eu.schubduese.fritzfx.fritz;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import de.mapoll.javaAVMTR064.Action;
import de.mapoll.javaAVMTR064.FritzConnection;
import de.mapoll.javaAVMTR064.Response;
import de.mapoll.javaAVMTR064.Service;
import eu.schubduese.common.config.Settings;
import eu.schubduese.common.utils.StreamUtils;
import eu.schubduese.fritzfx.gui.MainGuiController;
import eu.schubduese.fritzfx.gui.MainGuiController.PropKey;

public class FritzConnector
{

	FritzConnection 			fritzConnection;
	private String 				ip;
	private String 				user;
	private String 				password;
	private boolean 			isConnected;
	List<IFritzboxConnector> 	fritzboxConnectorList;
	private Timer 				statusTimer;
	private TimerTask 			statusTimerTask;
	private Timer 				statesTableTimer;
	private TimerTask 			statesTableTimerTask;
	private int					tr064Port;
	
	public FritzConnector()
	{
		super();
		setValues();
	}
	
	public void setValues()
	{
		this.ip = Settings.loadValue(MainGuiController.PropKey.FRITZBOXIP.getPropertiesValue());
		this.password = Settings.loadValue(MainGuiController.PropKey.FRITZBOXPASSWORT.getPropertiesValue());
		this.user = Settings.loadValue(MainGuiController.PropKey.FRITZBOXUSER.getPropertiesValue());
		this.tr064Port = Settings.loadIntValue(MainGuiController.PropKey.FRITZBOXPORT.getPropertiesValue());
	}
	
	public void connect()
	{
		try
		{
			getFritzConnection().init();
			getStatusTimer().scheduleAtFixedRate(getStatusTimerTask(), 30, Settings.loadIntValue(MainGuiController.PropKey.FRITZBOXACTRATE.getPropertiesValue(), 1000));
//			getStatesTableTimer().scheduleAtFixedRate(getStatesTableTimerTask(), 30, Settings.loadIntValue(PropKey.FRITZBOXACTRATE.getPropertiesValue(), 1000));
			setConnected(true);
//			getFritzAnrufListe();
		} catch (ClientProtocolException e)
		{
			e.printStackTrace();
			setConnected(false);
		} catch (IOException e)
		{
			e.printStackTrace();
			setConnected(false);
		} catch (JAXBException e)
		{
			e.printStackTrace();
			setConnected(false);
		}
	}
	
	public void disconnect()
	{
		getStatusTimer().cancel();
		setConnected(false);
	}
	
	protected Timer getStatusTimer()
	{
		if (statusTimer == null)
			statusTimer = new Timer("fritzboxstatusThread");
		return statusTimer;
	}

	protected TimerTask getStatusTimerTask()
	{
		if (statusTimerTask == null)
		{
			 statusTimerTask = new TimerTask() 
			 {

		            @Override
		            public void run() 
		            {
		    			Service service = getFritzConnection().getService("WANCommonInterfaceConfig:1");
		    			Map<String, Object> set = new HashMap<>();
		    			set.put("NewSyncGroupIndex",0);
		    			Action actionOnlineMonitor = service.getAction("X_AVM-DE_GetOnlineMonitor");
		    			Action actionCommonLinkProperties = service.getAction("GetCommonLinkProperties");
		    			WanStatusInformation wanStatusInformation = new WanStatusInformation();
		    			try
						{
		    				Response response = actionOnlineMonitor.execute(set);
		    				String bytesReceived = response.getValueAsString("Newds_current_bps");
		    				String bytesSend = response.getValueAsString("Newus_current_bps");
	    					wanStatusInformation.setBytesReceivedListe(values2IntListe(bytesReceived));
		    				wanStatusInformation.setBytesSendListe(values2IntListe(bytesSend));
		    				response = actionCommonLinkProperties.execute();
		    				wanStatusInformation.setMaxDownloadBitrate(response.getValueAsInteger("NewLayer1DownstreamMaxBitRate"));
		    				wanStatusInformation.setMaxUploadBitrate(response.getValueAsInteger("NewLayer1UpstreamMaxBitRate"));
		    				wanStatusInformation.setPhysicalLinkStatus(response.getValueAsString("NewPhysicalLinkStatus"));
		    				wanStatusInformation.setWanAccessType(response.getValueAsString("NewWANAccessType"));
						} catch (UnsupportedOperationException e)
						{
							setConnected(false);
							e.printStackTrace();
						} catch (IOException e)
						{
							setConnected(false);
							e.printStackTrace();
						} catch (NoSuchFieldException e)
						{
							setConnected(false);
							e.printStackTrace();
						}
		            	for (IFritzboxConnector iFritzboxConnector : getFritzboxConnectorList())
							iFritzboxConnector.setStatusInformation(wanStatusInformation);
		            }
		        };
		}
		return statusTimerTask;
	}
	
	protected Timer getStatesTableTimer()
	{
		if (statesTableTimer == null)
			statesTableTimer = new Timer("fritzboxstatesTableThread");
		return statesTableTimer;
	}
	
	protected TimerTask getStatesTableTimerTask()
	{
		if (statesTableTimerTask == null)
		{
			statesTableTimerTask = new TimerTask() 
			{
				
				@Override
				public void run() 
				{
					Service service = getFritzConnection().getService("Layer3Forwarding:1");
					Map<String, Object> set = new HashMap<>();
//					set.put("NewForwardingIndex", 0);
					Action action = service.getAction("ServiceStatesTable");
//					WanStatusInformation wanStatusInformation = new WanStatusInformation();
					try
					{
						Response response = action.execute(set);
						System.out.println(response);
//						String bytesReceived = response.getValueAsString("Newds_current_bps");
//						String bytesSend = response.getValueAsString("Newus_current_bps");
//						wanStatusInformation.setCurrentBytesReceived(values2IntListe(bytesReceived));
//						wanStatusInformation.setCurrentBytesSend(values2IntListe(bytesSend));
					} catch (UnsupportedOperationException e)
					{
						setConnected(false);
						e.printStackTrace();
					} catch (IOException e)
					{
						setConnected(false);
						e.printStackTrace();
					}
//					for (IFritzboxConnector iFritzboxConnector : getFritzboxConnectorList())
//					{
//						iFritzboxConnector.setStatusInformation(wanStatusInformation);
//					}
				}
			};
		}
		return statesTableTimerTask;
	}
	
	public List<FritzAnruf> getFritzAnrufListe()
	{
		List<FritzAnruf> anrufListe = new ArrayList<>();
		if (!isConnected())
			return anrufListe;
		Service service = getFritzConnection().getService("X_AVM-DE_OnTel:1");
		Map<String, Object> set = new HashMap<>();
		Action actionCallList = service.getAction("GetCallList");
		try
		{
			Response response = actionCallList.execute(set);
			String callListUrl = response.getData().get("NewCallListURL");
//			System.out.println(callListUrl);
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(callListUrl);
			HttpResponse httpResponse = client.execute(request);

			BufferedReader bufferedReader = new BufferedReader
			    (new InputStreamReader(httpResponse.getEntity().getContent()));

			String line = "";
			while ((line = bufferedReader.readLine()) != null) 
			    System.out.println(line);
			new StreamUtils().bufferedReader2File(bufferedReader, "/home/joerg/anrufliste.xml");
			
			//siehe hier: https://stackoverflow.com/questions/8186896/how-do-i-parse-this-xml-in-java-with-jaxb
			 JAXBContext jc = JAXBContext.newInstance(FritzAnruf.class);
		     Unmarshaller unmarshaller = jc.createUnmarshaller();
		     FritzAnruf fritzAnruf = (FritzAnruf) unmarshaller.unmarshal(bufferedReader);

		     Marshaller marshaller = jc.createMarshaller();
		     marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		     marshaller.marshal(fritzAnruf, System.out);
			
		} catch (UnsupportedOperationException e)
		{
			setConnected(false);
			e.printStackTrace();
		} catch (IOException e)
		{
			setConnected(false);
			e.printStackTrace();
		} catch (JAXBException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return anrufListe;
	}
	
	private List<Integer> values2IntListe(String s)
	{
		List<Integer> intListe = new ArrayList<>();
		String[] splitted = s.split(",");
		for (String string : splitted)
			intListe.add(Integer.parseInt(string));
		Collections.reverse(intListe);
		return intListe;
	}
		
	private void setConnected(boolean isConnected)
	{
		this.isConnected = isConnected;
		for (IFritzboxConnector iFritzboxConnector : getFritzboxConnectorList())
		{
			iFritzboxConnector.SetConnected(isConnected);
		}
	}

	public boolean isConnected()
	{
		return isConnected;
	}

	private FritzConnection getFritzConnection()
	{
		if (fritzConnection == null)
		{
			if (tr064Port > 0)
				fritzConnection = new FritzConnection(ip, tr064Port, user, password); 
			else
				fritzConnection = new FritzConnection(ip, user, password); 
		}
		return fritzConnection;
	}

	public void addIFritzboxConnector(IFritzboxConnector fritzboxConnector)
	{
		getFritzboxConnectorList().add(fritzboxConnector);
	}
	
	private List<IFritzboxConnector> getFritzboxConnectorList()
	{
		if (fritzboxConnectorList == null)
			fritzboxConnectorList = new ArrayList<>();
		return fritzboxConnectorList;
	}

}
