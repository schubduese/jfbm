package eu.schubduese.fritzfx.fritz;

public interface IFritzboxConnector
{
	
	public void SetConnected(boolean isConnected);
	
	public void setStatusInformation(WanStatusInformation wanStatusInformation);
}
