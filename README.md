# fritzfx
GUI zur Anzeige div. Fritzbox-Daten, Import/Export Telefonbuch

## Funktionen
  * Telefonbuch Import
  * Anzeige der aktuellen Bandbreite (auch als Tooltip in der Taskleiste)
  * Import von Owncloud/Nextcloud Daten (bisher nur über vcard-Datei)
  * allgemeine Info

## Kompilieren
  * Git-Repository clonen: 
  ```bash
git clone git@gitlab.com:schubduese/jfbm.git 
  ``` 
  * ins Verzeichnis wechseln, Java (OpenJDK) und Maven 3 muss vorhanden sein
  ```bash
  mvn package
  ```